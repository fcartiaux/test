# PROJECT NAME #

[![Build Status](https://www.bitrise.io/app/dd90da9ce0f63b21/status.svg?token=MvqSfHH1n9Z-65eB57IVQw&branch=develop)](https://www.bitrise.io/app/dd90da9ce0f63b21) ![platforms](https://img.shields.io/badge/platforms-iOS%20%7C%20Android%20-333333.svg) [![Slack channel](https://img.shields.io/badge/slack-Keyneo%20Group%20%2F%20General-blue.svg)](https://keyneosoft-team.slack.com/messages/C14R44TA6/)

-----------------------
# README #

Mettre ici une description de votre projet. Il sert à quoi / qui
Exemple de ce que l'on peut faire pour un readme utile.

[![Download on the AppStore](https://raw.github.com/repat/README-template/master/appstore.png)](https://itunes.apple.com/app/id123456)     [![Get it on Google Play](https://raw.github.com/repat/README-template/master/googleplay.png)](https://play.google.com/store/apps/details?id=com.package.path)

-----------------------
# REQUIREMENTS #

* Xcode 8.3.2 (8E2002)
* Swift 3
* Cocoapods 1.3.1

-----------------------
# LIENS UTILES #

* [Trello](https://www.google.fr)
* [Redmine](https://www.google.fr)
* [Mantis](https://www.google.fr)
* [Design](https://www.google.fr)

-----------------------
# SETUP #

Ce projet utilise Cocoapods pour gérer ses dépendances.

Pour connaitre votre version :

```
#!sh

pod --version
```

Placez vous dans le répertoire du projet avec le terminal.

```
#!sh

pod update --verbose
```

-----------------------
# COMPTES CLIENT #
 Dev              | Preprod          | Prod
:----------------:|:----------------:|:----------------:
 login / password | login / password | login / password
 login / password | login / password | login / password
 login / password | login / password | login / password

-----------------------
# LIBRAIRES #

* [RxSwift](https://github.com/ReactiveX/RxSwift)
* [RxCocoa](https://github.com/ReactiveX/RxSwift)
* [RealmSwift](https://realm.io/docs/swift/latest/)
* [Alamofire](https://github.com/Alamofire/Alamofire)

-----------------------
# LIVRAISON #
## iOS ##

```
#!sh

Mettre ici le processus de livraison.

@iOS mettre les différents certificats par platformes.

Attention à ne pas mettre de mdp à mon sens. Expliquer le systeme de TAG.

```

## Android ##

```
#!sh

Mettre ici le processus de livraison.

@Android : ???.

Attention à ne pas mettre de mdp à mon sens. Expliquer le systeme de TAG.

```

-----------------------
# INFORMATIONS CLIENT #
## Contact principal ##
* Pierre Dupont, 06.49.37.54.23, pierre.dupont@leclient.com

## Contacts secondaires ##
* Martin Dupond, 06.12.34.56, martin.dupond@leclient.com
* Justin Durand, 06.65.43.21, justin.durand@leclient.com

-----------------------
# CONTRIBUTEURS #
## Product Owner ##
* Fabrice Bellotti
## Chef de projet ##
* Aurélien Deschodt
## Lead dev ##
* Nicolas Zurini
## Développeurs ##
* Cartiaux François
* Nicolas Zurini
* Matthias De Bie
* Loic Albert